export function passwordRegEX(value) {
  return /^(?!.* )(?=.*[a-z])(?=.*[A-Z])(?=.*[*@=_$%&.]).{8,20}$/.test(value)
}
