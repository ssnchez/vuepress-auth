import Vuex from 'vuex'

import {
  CHANGE_LOGIN_STATUS,
  TOKEN,
  SEND_REQ_ACCESS,
  AUTH
} from '../vuex/mutationsTypes'

import Login from '../components/Login.vue'
import Logout from '../components/Logout.vue'
import store from '../vuex/store'

export default ({ Vue }) => {
  /** REGISTER THE STORE */
  Vue.use(Vuex)

  /** REGISTER LOGIN AND LOGOUT COMPONENTS */
  Vue.component('Login', Login)
  Vue.component('Logout', Logout)

  /** ALL THE LOGIC FOR PAGES */
  Vue.mixin({
    store,
    mounted() {
      const cheackAuth = async () => {
        const loginPath = '/login/'

        if (this.$store.state.isLoged && this.$route.path === '/logout/') {
          sessionStorage.clear()

          this.$store.commit(CHANGE_LOGIN_STATUS, false)

          this.$router.push('/').catch(() => true)
        } else if (
          !this.$store.state.isLoged &&
          this.$route.path !== loginPath
        ) {
          this.$router.push(loginPath).catch(() => true)
        } else if (
          this.$store.state.isLoged &&
          this.$route.path === loginPath
        ) {
          this.$router.push('/').catch(() => true)
        } else if (this.$route.path !== loginPath) {
          const token = sessionStorage.getItem(TOKEN)

          if (token !== '') {
            await this.$store.dispatch(SEND_REQ_ACCESS, token)

            if (!sessionStorage.getItem(AUTH)) {
              sessionStorage.clear()
              
              window.top.location.reload(true)
            }
          } else {
            sessionStorage.clear()
              
            window.top.location.reload(true)
          }
        }
      }

      cheackAuth()
    }
  })
}
