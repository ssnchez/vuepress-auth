// Copyright 2022 ssanchez
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const alg = (string, salt, decode = false) => {
  if (
    string &&
    salt &&
    decode
  ) {
    const decoded = string
      .split('')
      .map((letter, index) => {
        if (index % 2 !== 0) {
          return letter
        }
      })
      .join('')

    return decoded.trim() === salt.trim()
  } else if (string && salt) {
    const arr1 = string.split('')
    const arr2 = salt.split('')

    const result = arr1
      .map((letter, index) => [letter, arr2[index]])
      .flat()
      .join('')
    
    return result
  }

  return false
}

export default alg
