const CryptoJS = require('crypto-js')

function toBase64String (words) {
  return CryptoJS.enc.Base64.stringify(words)
}

function toWordArray (words) {
  return CryptoJS.enc.Base64.parse(words.toString())
}

export const encrypter = (password, ENC_KEY) => {
  const iv = CryptoJS.lib.WordArray.random(12)

  const encrypter = CryptoJS.AES.encrypt(password, ENC_KEY, {
    iv : iv,
    padding: CryptoJS.pad.ZeroPadding
  })

  const encBinArray = toWordArray(encrypter)
  const cipherArray = iv
  
  return toBase64String(cipherArray.concat(encBinArray))
}
