window.global = window

const {
  JWS,
  JWE,
  JWK
} = require('node-jose')

const moment = require('moment')
const faker = require('@faker-js/faker').default
const encryptionAndContentAlg = 'A256GCM'
const signAlg = 'RS256'
const encryptionAlg = 'RSA-OAEP-256'
const fileFormat = 'pem'

const jwtData = {
  headers: {
    alg: 'RS256',
    typ: 'JWT'
  },
  payload: {
    aud: 'Documentation_Portals',
    exp: null,
    content: {
      channel: 'WEB',
      appId: 'TODO1',
      appModule: 'Login',
      transactionId: faker.datatype.uuid(),
      lastAccessDate: null,
      lang: 'ES',
      user: {
        uId: faker.datatype.uuid(),
        userName: null,
        password: null
      }
    }
  }
}

/** Read and prepare the encryption keys */
const prepareKeys = async () => {
  const keyStore = JWK.createKeyStore();

  const privateToSign = await keyStore.add(process.env.PRIVATE_KEY, fileFormat, { alg: signAlg, kid: 'private-to-sign' });
  const publicToEncrypt = await keyStore.add(process.env.PUBLIC_KEY, fileFormat, { alg: encryptionAlg, kid: 'public-to-encrypt' });
  const privateToDecrypt = await keyStore.add(process.env.PRIVATE_KEY, fileFormat, { alg: encryptionAlg, kid: 'private-to-decrypt' });
  const publicToVerify = await keyStore.add(process.env.PUBLIC_KEY, fileFormat, { alg: signAlg, kid: 'public-to-verify' });

  return {
    privateToSign,
    publicToEncrypt,
    privateToDecrypt,
    publicToVerify
  }
}

/** Sign method */
const sign = async (keys, data) => {
  const signedData = await JWS
    .createSign({
      alg: signAlg,
      format: 'compact',
      fields: data.headers
    }, keys.privateToSign)
    .update(JSON.stringify(data.payload))
    .final()

  return signedData
}

/** Encrypt method */
const encrypt = async (keys, data) => {
  const encryptedData = await JWE
    .createEncrypt({
      contentAlg: encryptionAndContentAlg,
      format: 'compact',
      fields: {
        alg: encryptionAlg,
        cty: 'JWT',
        enc: encryptionAndContentAlg,
        iss: 'TODO1'
      }
    }, keys.publicToEncrypt)
    .update(data)
    .final()

  return encryptedData
}

/** Generate JWT Method */
export const jwtCreator = async (userName, password) => {
  try {
    const currenDate = new Date()
    const newPayload = jwtData.payload

    /** Override all the necesary current values of the jwtData */
    newPayload.exp = moment(currenDate).add(parseInt(process.env.EXPIRY_DAYS, 10), 'days').unix()
    newPayload.content.lastAccessDate = moment(currenDate).format('yyyyMMddHHmmss')
    newPayload.content.user.userName = userName
    newPayload.content.user.password = password

    const newJwt = {
      ...jwtData,
      payload: {
        ...jwtData.payload,
        content: JSON.stringify(newPayload.content)
      }
    }

    const generatedKeys = await prepareKeys()
    const signedJWT = await sign(generatedKeys, newJwt)
    const encryptedJWT = await encrypt(generatedKeys, signedJWT)

    return encryptedJWT
  } catch (error) {
    return error
  }
}

export const jwtValidator = async (token) => {
  try {
    const keys = await prepareKeys()
    const jwtDecrypt = await JWE.createDecrypt(keys.privateToDecrypt).decrypt(token)
    const { payload } = await JWS.createVerify(keys.publicToVerify)
      .verify(jwtDecrypt.payload.toString())

    const { user } = JSON.parse(JSON.parse(payload.toString()).content)

    return { 
      success: true, 
      user: user.userName, 
      password: user.password,
      tenantId: user.tenantId 
    }
  } catch (error) {
    return { success: false, msg: error }
  }
}
