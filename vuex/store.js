// Copyright 2022 ssanchez
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Vue from 'vue'
import Vuex from 'vuex'
import md5 from 'md5'

import axios from 'axios'

import {
  CHANGE_LOGIN_STATUS,
  SESSION_SOTARE_LOGIN,
  SHOW_LOGIN_ERROR,
  SALT,
  SEND_CREDENTIALS_TO_API,
  ADD_ERROR_MESSAGE,
  TOKEN,
  SEND_REQ_ACCESS,
  CHANGE_AUTH_STATUS,
  AUTH
} from './mutationsTypes'

import alg from '../utils/alg'
import { encrypter } from '../utils/encrypter'
import { jwtValidator } from '../utils/jwtCreator'

Vue.use(Vuex)

window.global = window

const chance = require('chance')()

const state = {
  isLoged: sessionStorage.getItem(SESSION_SOTARE_LOGIN),
  salt: sessionStorage.getItem(SALT),
  showLoginError: false,
  errorMsg: null,
  token: sessionStorage.getItem(TOKEN),
  auth: sessionStorage.getItem(AUTH)
}

const mutations = {
  [SHOW_LOGIN_ERROR](state, showLoginError) {
    state.showLoginError = showLoginError
  },
  [CHANGE_LOGIN_STATUS](state, loginStatus) {
    state.isLoged = loginStatus
  },
  [ADD_ERROR_MESSAGE](state, errorMsg) {
    state.errorMsg = errorMsg
  },
  [CHANGE_AUTH_STATUS](state, authstatus) {
    state.auth = authstatus
  }
}

const actions = {
  [CHANGE_LOGIN_STATUS]({ commit }, { loginHash, salt, token, user, pass }) {
    if (loginHash) {
      sessionStorage.setItem(SESSION_SOTARE_LOGIN, alg(loginHash, salt))
      sessionStorage.setItem(SALT, salt)
      sessionStorage.setItem(TOKEN, token)

      commit(CHANGE_LOGIN_STATUS, loginHash)
    }
  },
  [SEND_CREDENTIALS_TO_API]({ dispatch, commit }, credentials){
    const newCredentials = {
      ...credentials,
      password: encrypter(credentials.password, process.env.ENC_KEY),
      tenant: process.env.TENANT_ID
    }

    axios.post(`${process.env.API}:${process.env.PORT}/api/login`, newCredentials)
      .then((data)=>{
        if (data.data.success) {
          const salt = md5(chance.name({ prefix: true }))
          const md5Credentials = md5(JSON.stringify(newCredentials))

          dispatch(CHANGE_LOGIN_STATUS, { 
            loginHash: md5Credentials,
            salt: salt,
            token: data.data.token,
            user: encrypter(newCredentials.userName, process.env.ENC_KEY),
            pass: newCredentials.password
          })

          window.top.location.reload()
        } else {
          commit(SHOW_LOGIN_ERROR, true)
          commit(ADD_ERROR_MESSAGE, data.data.error)
        }
      })
      .catch((error) => {
        commit(SHOW_LOGIN_ERROR, true)
        commit(ADD_ERROR_MESSAGE, error.response?.data?.error || error.message)
      })
  },
  async [SEND_REQ_ACCESS]({ dispatch, commit }, token) {
    const info = await jwtValidator(token)

    if (
      info.success &&
      info.tenantId === process.env.TENANT_ID
    ) {
      dispatch(CHANGE_AUTH_STATUS, true)
    } else {
      sessionStorage.clear()

      commit(CHANGE_AUTH_STATUS, false)
    }
  },
  async [CHANGE_AUTH_STATUS]({ commit }, authstatus) {
    sessionStorage.setItem(AUTH, authstatus)

    commit(CHANGE_AUTH_STATUS, authstatus)
  }
}

const store = new Vuex.Store({
  state, 
  mutations,
  actions
})

export default store
