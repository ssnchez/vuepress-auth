// Copyright 2022 ssanchez
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
export const SALT = 'SALT'
export const TOKEN = 'TOKEN'
export const AUTH = 'AUTH'
export const FIELD_A = 'FIELD_A'
export const FIELD_B = 'FIELD_B'
export const SESSION_SOTARE_LOGIN = 'ISLOGGED'

export const SHOW_LOGIN_ERROR = 'SHOWLOGINERROR'
export const CHANGE_LOGIN_STATUS = 'CHANGELOGINSTATUS'
export const SEND_CREDENTIALS = 'SENDCREDENTIALS' 
export const SEND_CREDENTIALS_TO_API = 'SENDCREDENTIALSTOAPI'
export const ADD_ERROR_MESSAGE = 'ADDERRORMESSAGE'
export const SEND_REQ_ACCESS = 'SENDREQACCESS'
export const CHANGE_AUTH_STATUS = 'CHANGEAUTHSTATUS'
